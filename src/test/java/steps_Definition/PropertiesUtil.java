package steps_Definition;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class PropertiesUtil {


    public static Properties load(String fileName) {
        Properties properties = new Properties();
        if (fileName == null) {
            throw new IllegalArgumentException("FileName is not set.");
        } else {
            // Please change this try catch block later
            InputStream is = PropertiesUtil.class.getResourceAsStream(fileName);
            try {
                properties.load(new BufferedInputStream(is));
            } catch (IOException ex) {
                System.out.println(ex.toString());
                System.out.println("Cannot load properties file " + fileName);
            }
        }
        return properties;
    }

    public static String getProperty(String propertyFile, String propertyName) {
        String propertyValue = System.getProperty(propertyName);
        if (propertyValue == null) {
            Properties properties = PropertiesUtil.load(propertyFile);
            propertyValue = properties.getProperty(propertyName);
        }
        return propertyValue;
    }


    public static String getPropertyFromAbsoluteFilePath(String filePath, String propertyKey) {
        Properties props = new Properties();
        String propertyValue = "";
        FileInputStream in = null;
        try {
            in = new FileInputStream(filePath);
            props.load(in);
            propertyValue = props.getProperty(propertyKey);
            in.close();
        } catch (IOException e) {
            System.out.println(e.toString());
        }
        return propertyValue;
    }
}
