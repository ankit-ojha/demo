package steps_Definition;

import java.io.File;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

public class StepDef {

    File directory;

    @Test
    public void ex2() {
        System.out.println("From properties file :: " + getDownloadLocation());
        System.out.println("Injected Bamboo variable :: " + System.getProperty("host"));
        String path = System.getProperty("user.dir") + "\\src\\test\\resources\\download";
        directory = new File(path);
        sizeOfDirectory(directory);
//        createDirectory(directory);
        lastModifiedTime(directory);
//        deleteAllFiles(directory);
    }

    private String getDownloadLocation() {
        return PropertiesUtil.getProperty("/test.properties", "DOWNLOAD_LOCATION");
    }

    private void deleteAllFiles(File dir) {
        boolean success = false;
        if (dir.isDirectory()) {
            for (File deleteMe : dir.listFiles()) {
                deleteAllFiles(deleteMe);
            }
        }
        if (dir.isFile()) {
            success = dir.delete();
            if (success) {
                System.out.println(dir.getAbsoluteFile() + " deleted");
            } else {
                System.out.println(dir.getAbsoluteFile() + " deletion failed!!!");
            }

        } else {
            System.out.println("'" + dir + "' directory was not deleted");
        }
    }

    public void createDirectory(File file) {
        boolean isDirectoryCreated = file.mkdir();
        if (isDirectoryCreated) {
            System.out.println(file.getAbsolutePath() + " created successfully");
        } else {
            System.out.println(file.getAbsolutePath() + " already exists");
        }
    }
    
    public void lastModifiedTime(File file){
        System.out.println("last modifed:" + new Date(file.lastModified()));
    }
    
    public void sizeOfDirectory(File file){
        long size = FileUtils.sizeOfDirectory(file);
        FileUtils.sizeOf(file);
        System.out.println("Size: " + size + " bytes");
    }

}
